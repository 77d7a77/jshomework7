let userArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let secondArray = ["1", "2", "3", "sea", "user", 23];

function createElementInList(array, parent) {
    if ( typeof parent === 'undefined' ) {
        let listArray = array.map((element) => {
            let listItem = document.createElement("li");
            listItem.innerHTML = `${element}`;
            return document.body.append(listItem);
        });
        return listArray;
    } else {
        let listArray = array.map((element) => {
            let listItem = document.createElement("li");
            listItem.innerHTML = `${element}`;
            let userPosition = document.querySelector(`${parent}`)
            return userPosition.append(listItem);
        });
        return listArray;
    }
}
createElementInList(userArray);
